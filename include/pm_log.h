#ifndef __PM_LOG_H__
#define __PM_LOG_H__

#include <oneos_config.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if !defined(PM_LOG_TAG)
#define PM_LOG_TAG "PM_TAG"
#endif

#ifdef OS_USING_DLOG
#include <dlog.h>

/* Debug contorol by dlog config */
#define ERROR(fmt, ...) LOG_E(PM_LOG_TAG, fmt, ##__VA_ARGS__)

#define WARN(fmt, ...) LOG_W(PM_LOG_TAG, fmt, ##__VA_ARGS__)

#define INFO(fmt, ...) LOG_I(PM_LOG_TAG, fmt, ##__VA_ARGS__)

#define DEBUG(fmt, ...) LOG_D(PM_LOG_TAG, fmt, ##__VA_ARGS__)

#else /* Not define OS_USING_DLOG, using kernel printf */
#include <os_util.h>

#if !defined(PM_LOG_LVL)
#define PM_LOG_LVL PM_LOG_INFO
#endif

#define PM_LOG_ERROR   (3) /* Error conditions */
#define PM_LOG_WARNING (4) /* Warning conditions */
#define PM_LOG_INFO    (6) /* Informational */
#define PM_LOG_DEBUG   (7) /* Debug-level messages */

#if (PM_LOG_ERROR <= PM_LOG_LVL)
#define ERROR(fmt, ...) os_kprintf("[ERROR] [%s] " fmt "\r\n", PM_LOG_TAG, ##__VA_ARGS__);
#else
#define ERROR(fmt, ...)
#endif

#if (PM_LOG_WARNING <= PM_LOG_LVL)
#define WARN(fmt, ...) os_kprintf("[WARN] [%s] " fmt "\r\n", PM_LOG_TAG, ##__VA_ARGS__);
#else
#define WARN(fmt, ...)
#endif

#if (PM_LOG_INFO <= PM_LOG_LVL)
#define INFO(fmt, ...) os_kprintf("[INFO] [%s] " fmt "\r\n", PM_LOG_TAG, ##__VA_ARGS__);
#else
#define INFO(fmt, ...)
#endif

#if (PM_LOG_DEBUG <= PM_LOG_LVL)
#if (defined(PM_USING_VOLTAGE_MONITOR) && (OS_TIMER_TASK_STACK_SIZE < 512))
#error "please increase the OS_TIMER_TASK_STACK_SIZE for Voltage Monitor"
#endif

#include <stdio.h>
#define DEBUG(fmt, ...) printf("[DEBUG] [%s] " fmt "\r\n", PM_LOG_TAG, ##__VA_ARGS__);
#else
#define DEBUG(fmt, ...)
#endif

#endif /* OS_USING_DLOG */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __PM_LOG_H__ */
