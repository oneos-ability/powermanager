/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_voltage_monitor.h
 *
 * @brief       Voltage monitor api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-9    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __PM_VM_H__
#define __PM_VM_H__

#include <oneos_config.h>

#ifdef PM_USING_VOLTAGE_MONITOR
#include <os_types.h>
#include <os_stddef.h>
#include <float.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef float (*pm_vm_temp_cb_t)(float temperature);
typedef void (*pm_vm_notify_cb_t)(int32_t voltage);

typedef struct pm_vm_cfg
{
    float             calibration_coefficient;
    int32_t        calibration_base;
    uint32_t       sampling_duration_sec;
    pm_vm_temp_cb_t   temp_correction_cb; /* User's temperature correction callback, NULL is allowed. */
    pm_vm_notify_cb_t alert_notify_cb;    /* If the battery voltage correction value consecutive lower than the alarm
                                             voltage, an alarm will be issued. */
} pm_vm_cfg_t;

os_err_t pm_voltage_monitor_on(int32_t threshold_mv, pm_vm_cfg_t *optional_cfg);
os_err_t pm_voltage_monitor_off(void);

os_err_t pm_vm_set_voltage_threshold(int32_t threshold_mv);
os_err_t pm_vm_set_calibration_coefficient(float calibration_coefficient);
os_err_t pm_vm_set_calibration_base(int32_t calibration_base);
os_err_t pm_vm_set_temp_correction_cb(pm_vm_temp_cb_t temp_correction_cb);

int32_t pm_vm_get_latest_data(void);
int32_t pm_vm_get_history_data(int32_t *data_array, os_size_t history_count);
os_err_t   pm_vm_clear_history(void);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PM_USING_VOLTAGE_MONITOR */
#endif /* __PM_VM_H__ */
