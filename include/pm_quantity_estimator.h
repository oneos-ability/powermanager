/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_quantity_estimator.h
 *
 * @brief       Quantity estimator api declaration
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-9    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __PM_VM_H__
#define __PM_VM_H__

#include <oneos_config.h>

#ifdef PM_USING_QUANTITY_ESTIMATOR
#include <os_types.h>
#include <os_stddef.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct pm_qe_power_consumption_list
{
    char        name[OS_NAME_MAX + 1];
    uint32_t working_current;
} pm_qe_dev_list_t;

os_err_t  pm_quantity_estimator_on(uint32_t capacity, uint32_t low_battery_percentage);
os_err_t  pm_quantity_estimator_off(void);
os_bool_t pm_quantity_estimator_is_on(void);

os_err_t  pm_qe_reset(uint32_t capacity, uint32_t low_battery_percentage);
os_bool_t pm_qe_is_low(uint32_t unstated_using);

/* Preview function */
#ifdef PM_QE_USING_PREVIEW_FUNC
int32_t pm_qe_used_get(void);
int32_t pm_qe_left_get(uint32_t unstated_using);
#endif /* PM_QE_USING_PREVIEW_FUNC */
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PM_USING_QUANTITY_ESTIMATOR */
#endif /* __PM_VM_H__ */
