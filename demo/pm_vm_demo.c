/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_vm_demo.c
 *
 * @brief       Voltage Monitor demo
 *
 * @revision
 * Date         Author          Notes
 * 2021-07-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>

#ifdef PM_USING_VOLTAGE_MONITOR

#include <stdio.h>
#include <pm_voltage_monitor.h>
#include <os_types.h>
#include <os_errno.h>

#ifdef OS_USING_SHELL
#include <shell.h>
#endif /* OS_USING_SHELL */

/*****************************************************************************
 *                          init configuration                               *
 *****************************************************************************/
#define VM_CAL_COEFFICIENT   (2.0)
#define VM_CAL_BASE          (0)
#define VM_DURATION          (10)
#define VM_VOLTAGE_THRESHOLD (3301)

#define TEST_LOG(fmt, ...) printf("[TEST_LOG] %s-%d: " fmt "\r\n", __func__, __LINE__, ##__VA_ARGS__);

float temp_cb(float temperature)
{
    TEST_LOG("temperature: %f", temperature);

    return (temperature >= 27) ? 1.0 : 0.7;
}

void aleart_cb(int32_t voltage)
{
    TEST_LOG("aleart voltage:%d", voltage);
}

os_err_t voltage_monitor_on(int32_t argc, char **argv)
{
    pm_vm_cfg_t cfg = {
        .calibration_coefficient = VM_CAL_COEFFICIENT,
        .calibration_base        = VM_CAL_BASE,
        .sampling_duration_sec   = VM_DURATION,
        .temp_correction_cb      = temp_cb,
        .alert_notify_cb         = aleart_cb,
    };

    os_err_t result = pm_voltage_monitor_on(VM_VOLTAGE_THRESHOLD, &cfg);
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

os_err_t voltage_monitor_off(int32_t argc, char **argv)
{
    os_err_t result = pm_voltage_monitor_off();
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

os_err_t vm_get_latest_data(int32_t argc, char **argv)
{
    int data = pm_vm_get_latest_data();

    TEST_LOG("Voltage[%d]-%s", data, (0 < data) ? "SUCCESS" : "FAILED");
    return OS_SUCCESS;
}

os_err_t vm_get_history_data(int32_t argc, char **argv)
{
    int cnt                     = 0;
    int history[PM_VM_DATA_MAX] = {0};

    cnt = pm_vm_get_history_data(history, PM_VM_DATA_MAX);

    printf("Voltage history - ");

    for (int i = 0; i < PM_VM_DATA_MAX; i++)
    {
        printf("[%d] ", history[i]);
    }
    printf("\r\n");

    return (0 >= cnt) ? OS_FAILURE : OS_SUCCESS;
}

os_err_t vm_update_configure(int32_t argc, char **argv)
{
    os_err_t result = OS_SUCCESS;

    result = pm_vm_set_voltage_threshold(32);
    TEST_LOG("pm_vm_set_voltage_threshold-%s", (OS_SUCCESS == result) ? "SUCCESS" : "FAILED");

    result = pm_vm_set_calibration_coefficient(1.2);
    TEST_LOG("pm_vm_set_calibration_coefficient-%s", (OS_SUCCESS == result) ? "SUCCESS" : "FAILED");

    result = pm_vm_set_calibration_base(-1);
    TEST_LOG("pm_vm_set_calibration_base-%s", (OS_SUCCESS == result) ? "SUCCESS" : "FAILED");

    result = pm_vm_set_temp_correction_cb(OS_NULL);
    TEST_LOG("pm_vm_set_temp_correction_cb-%s", (OS_SUCCESS == result) ? "SUCCESS" : "FAILED");

    return result;
}

os_err_t vm_clear_history()
{
    os_err_t result = pm_vm_clear_history();
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

#ifdef OS_USING_SHELL
SH_CMD_EXPORT(pm_vm_on, voltage_monitor_on, "voltage monitor on");
SH_CMD_EXPORT(pm_vm_off, voltage_monitor_off, "voltage monitor off");
SH_CMD_EXPORT(pm_vm_data, vm_get_latest_data, "vm get latest data");
SH_CMD_EXPORT(pm_vm_history, vm_get_history_data, "vm get history data");
SH_CMD_EXPORT(pm_vm_config, vm_update_configure, "vm update configuration");
SH_CMD_EXPORT(pm_vm_clear, vm_clear_history, "vm clear history");
#endif /* OS_USING_SHELL */

#endif /* PM_USING_VOLTAGE_MONITOR */
