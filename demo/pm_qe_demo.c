/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_vm_demo.c
 *
 * @brief       Quantity Estimator demo
 *
 * @revision
 * Date         Author          Notes
 * 2021-07-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>

#ifdef PM_USING_QUANTITY_ESTIMATOR

#include <stdio.h>
#include <pm_quantity_estimator.h>
#include <os_errno.h>
#include <device.h>

#ifdef OS_USING_SHELL
#include <shell.h>
#endif /* OS_USING_SHELL */

/*****************************************************************************
 *                          init configuration                               *
 *****************************************************************************/
#define PM_QE_BATTERY_CAPACITY    (4500)
#define PM_QE_UNSTATED_USING      (10)
#define PM_QE_CRITICAL_PERCENTAGE (20)
#define PM_QE_TEST_DEV_NAME       "adc1"

#define DEMO_TIMEOUT_SEC     (20)
#define DEMO_FAL_NAME        "pm_vm"
#define DEMO_FAL_DATA_SIZE   (sizeof(int32_t))
#define DEMO_FAL_DATA_OFFSET (0)
#define DEMO_UNSTATED_USING  (0)

static os_device_t *dev = OS_NULL;

#define TEST_LOG(fmt, ...) printf("[TEST_LOG] %s: " fmt "\r\n", __func__, ##__VA_ARGS__);

os_err_t pm_qe_on_sh(int32_t argc, char **argv)
{
    os_err_t result = pm_quantity_estimator_on(PM_QE_BATTERY_CAPACITY, PM_QE_CRITICAL_PERCENTAGE);
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

os_err_t pm_qe_off_sh(int32_t argc, char **argv)
{
    os_err_t result = pm_quantity_estimator_off();
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

os_err_t pm_qe_is_on_sh(int32_t argc, char **argv)
{
    TEST_LOG("QE is %s", pm_quantity_estimator_is_on() ? "ON" : "OFF");
    return OS_SUCCESS;
}

os_err_t pm_qe_reset_sh(int32_t argc, char **argv)
{
    os_err_t result = pm_qe_reset(PM_QE_BATTERY_CAPACITY, PM_QE_CRITICAL_PERCENTAGE);
    TEST_LOG("%s", OS_SUCCESS == result ? "SUCCESS" : "FAILED");
    return result;
}

#ifdef PM_QE_USING_PREVIEW_FUNC
os_err_t pm_qe_battery_used_get_sh(int32_t argc, char **argv)
{
    int32_t battery_used = pm_qe_used_get();
    if (0 > battery_used)
    {
        TEST_LOG("Battery used get failed.");
    }
    else
    {
        TEST_LOG("Battery used: [%d]mAh", battery_used);
    }

    return OS_SUCCESS;
}

os_err_t pm_qe_remaining_battery_get_sh(int32_t argc, char **argv)
{
    int32_t remaining_battery = pm_qe_left_get(PM_QE_UNSTATED_USING);
    if (0 > remaining_battery)
    {
        TEST_LOG("Low battery!");
    }
    else
    {
        TEST_LOG("Remaining battery: [%d]mAh", remaining_battery);
    }

    return OS_SUCCESS;
}
#endif /* PM_QE_USING_PREVIEW_FUNC */

os_err_t pm_qe_is_low_battery_sh(int32_t argc, char **argv)
{
    TEST_LOG("QE is %s", pm_qe_is_low(PM_QE_UNSTATED_USING) ? "LOW BATTERY" : "STABLE");
    return OS_SUCCESS;
}

os_err_t pm_qe_open_dev_sh(int32_t argc, char **argv)
{
    os_err_t result = OS_SUCCESS;

    TEST_LOG("Device name:[%s]", PM_QE_TEST_DEV_NAME);
    dev = os_device_find(PM_QE_TEST_DEV_NAME);
    if (OS_NULL == dev)
    {
        TEST_LOG("Can not find device[%s].", PM_QE_TEST_DEV_NAME);
        return OS_FAILURE;
    }

    result = os_device_open(dev);
    if (OS_SUCCESS != result)
    {
        TEST_LOG("device[%s] open failed!", PM_QE_TEST_DEV_NAME);
        return result;
    }

    return result;
}

os_err_t pm_qe_close_dev_sh(int32_t argc, char **argv)
{
    os_err_t result = OS_SUCCESS;

    if (OS_NULL == dev)
    {
        TEST_LOG("Can not find device.");
        return OS_FAILURE;
    }

    result = os_device_close(dev);
    if (OS_SUCCESS != result)
    {
        TEST_LOG("device[%s] close failed!", dev->name);
    }

    return result;
}

#ifdef PM_QE_USING_PREVIEW_FUNC
#include <os_assert.h>
#include <os_timer.h>
#include <drv_cfg.h>

void pm_qe_perodic_cb(void *parameter)
{
    int32_t  battery_used = 0;
    int32_t  fal_data_get = 0;
    int32_t  fal_cnt      = 0;
    fal_part_t *fal_part     = OS_NULL;

    /* Find FAL part by name */
    fal_part = fal_part_find(DEMO_FAL_NAME);
    OS_ASSERT_EX(OS_NULL != fal_part, "Please add fal part first");

    /* Make sure QE is on */
    if (!pm_quantity_estimator_is_on())
    {
        TEST_LOG("QE is off!");
        return;
    }

    /* Get Battery power used */
    battery_used = pm_qe_used_get();
    TEST_LOG("Used battery: [%d]mAh", battery_used);

    fal_cnt = fal_part_erase_all(fal_part);
    OS_ASSERT(0 <= fal_cnt);

    /* Write data to flash */
    fal_cnt = fal_part_write(fal_part, DEMO_FAL_DATA_OFFSET, (uint8_t *)&battery_used, DEMO_FAL_DATA_SIZE);
    if (DEMO_FAL_DATA_SIZE != fal_cnt)
    {
        TEST_LOG("FAL write failed!");
        return;
    }

    TEST_LOG("FAL write success!");

    /* Read data from flash */
    fal_cnt = fal_part_read(fal_part, DEMO_FAL_DATA_OFFSET, (uint8_t *)&fal_data_get, DEMO_FAL_DATA_SIZE);
    if (DEMO_FAL_DATA_SIZE != fal_cnt)
    {
        TEST_LOG("FAL read failed.");
        return;
    }

    TEST_LOG("Data from flash:[%d]", fal_data_get);
}

os_err_t pm_qe_demo_periodcally_sh(int32_t argc, char **argv)
{
    os_err_t    result      = OS_SUCCESS;
    os_timer_id pm_vm_timer = OS_NULL;

    /* Create & start timer */
    pm_vm_timer = os_timer_create(NULL,
                                  DEMO_FAL_NAME,
                                  pm_qe_perodic_cb,
                                  OS_NULL,
                                  DEMO_TIMEOUT_SEC * OS_TICK_PER_SECOND,
                                  OS_TIMER_FLAG_PERIODIC);

    if (OS_NULL == pm_vm_timer)
    {
        TEST_LOG("Timer failed to create!");
        return OS_FAILURE;
    }

    result = os_timer_start(pm_vm_timer);
    if (OS_SUCCESS != result)
    {
        TEST_LOG("Timer failed to start!");
        return result;
    }

    /* Make sure the PE is on */
    if (!pm_quantity_estimator_is_on())
    {
        result = pm_quantity_estimator_on(PM_QE_BATTERY_CAPACITY, PM_QE_CRITICAL_PERCENTAGE);
        OS_ASSERT(OS_SUCCESS == result);
    }

    return result;
}
#endif /* PM_QE_USING_PREVIEW_FUNC */

#ifdef OS_USING_SHELL
SH_CMD_EXPORT(pm_qe_on, pm_qe_on_sh, "Call pm_quantity_estimator_on");
SH_CMD_EXPORT(pm_qe_off, pm_qe_off_sh, "Call pm_quantity_estimator_off");
SH_CMD_EXPORT(pm_qe_is_on, pm_qe_is_on_sh, "Call pm_quantity_estimator_is_on");
SH_CMD_EXPORT(pm_qe_reset, pm_qe_reset_sh, "Call pm_qe_reset");
SH_CMD_EXPORT(pm_qe_is_low, pm_qe_is_low_battery_sh, "Call pm_qe_is_low_battery");
SH_CMD_EXPORT(pm_qe_dev_open, pm_qe_open_dev_sh, "Call pm_qe_open_dev_sh");
SH_CMD_EXPORT(pm_qe_dev_close, pm_qe_close_dev_sh, "Call pm_qe_close_dev_sh");

#ifdef PM_QE_USING_PREVIEW_FUNC
SH_CMD_EXPORT(pm_qe_used, pm_qe_battery_used_get_sh, "Call pm_qe_battery_used_get");
SH_CMD_EXPORT(pm_qe_remaining, pm_qe_remaining_battery_get_sh, "Call pm_qe_remaining_battery_get");
/* Demo for periodc get & write data to flash */
SH_CMD_EXPORT(pm_qe_demo_periodcally, pm_qe_demo_periodcally_sh, "Demo of get & write data to flash periodcally");
#endif /* PM_QE_USING_PREVIEW_FUNC */

#endif /* OS_USING_SHELL */
#endif /* PM_USING_QUANTITY_ESTIMATOR */
