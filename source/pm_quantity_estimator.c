/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_quantity_estimator.c
 *
 * @brief       Implements of quantity estimator, just for solution testing.
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-9    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <pm_quantity_estimator.h>
#include <pm_qe_cfg.c>
#include <device.h>
#include <os_assert.h>
#include <os_list.h>
#include <os_mutex.h>
#include <os_clock.h>
#include <os_memory.h>
#include <drv_cfg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#ifndef PM_LOG_LVL
#define PM_LOG_LVL PM_QE_LOG_LVL
#endif /* PM_LOG_LVL */
#include <pm_log.h>

#ifndef OS_USING_RTC
#error "PowerManager-QuantityEstimator: RTC not available."
#endif /* OS_USING_RTC */

/* XXX */
// #if defined(PM_QE_AUTO_CREATE) && (OS_MAIN_TASK_STACK_SIZE < 610) && defined(PM_QE_LOG_LVL_D)
// #error "PowerManager-QuantityEstimator: main task size not enough."
// #endif

typedef enum os_dev_ops_type pm_qe_dev_ops_t;
typedef struct lpmgr_notify  pm_qe_lpmgr_cb_t;

#define PM_QE_TAG             "PM_QE"
#define PM_QE_NORMAL_DEV_NAME "normal"
#define PM_QE_RTC_NAME        "rtc"
#define PM_QE_EPSILON         (1e-5)
#define PM_QE_SEC_PER_HOUR    (3600)

/* FIXME: lpmgr not supported yet until bsp lpmgr fixed. micro change to OS_USING_TICKLESS_LPMGR */
#ifdef QE_USING_TICKLESS_LPMGR
#define PM_QE_LPMGR_DEV_NAME OS_LPMGR_DEVICE_NAME
static os_device_t lpmgr_dev = {.name = PM_QE_LPMGR_DEV_NAME};
#endif /* QE_USING_TICKLESS_LPMGR */

typedef struct pm_qe_dev_info
{
    os_device_t    *dev;
    time_t          previous_run_time;
    time_t          latest_open_time;
    uint32_t        working_current;
    os_bool_t       dev_is_open;
    os_tick_t       lpm_deduction; /* device: [lpm deduction] / lpm: [duration] */
    os_slist_node_t info_node;
} pm_qe_dev_info_t;

typedef struct pm_qe
{
    uint32_t        capacity;
    uint32_t        percentage;
    time_t          qe_start_time;
    os_slist_node_t qe_slist_head;
    os_mutex_id     qe_mutex;
#ifdef QE_USING_TICKLESS_LPMGR
    os_tick_t         qe_lpm_latest_open_time;
    pm_qe_lpmgr_cb_t *qe_lpmgr_cb_info;
#endif /* QE_USING_TICKLESS_LPMGR */
} pm_qe_t;

static const int gs_pm_qe_dev_list_size = (sizeof(pm_qe_dev_list) / sizeof(pm_qe_dev_list_t));
static pm_qe_t  *gs_pm_qe_instance      = OS_NULL;

static os_err_t    pm_qe_info_find_and_update(os_device_t *dev, os_bool_t dev_is_open, os_bool_t is_lpm);
static os_bool_t   pm_qe_find_reg_dev(char *dev_name);
static uint32_t pm_qe_find_dev_current(char *dev_name);

static void pm_qe_lock(os_mutex_id mutex)
{
    os_mutex_lock(mutex, OS_WAIT_FOREVER);
}

static void pm_qe_unlock(os_mutex_id mutex)
{
    os_mutex_unlock(mutex);
}

os_err_t pm_qe_device_cb(os_device_t *dev, os_ubase_t event, os_ubase_t args)
{
    OS_ASSERT(OS_NULL != dev);

    os_bool_t dev_is_open = OS_FALSE;

    if (0 == strcmp(dev->name, PM_QE_RTC_NAME))
        return OS_SUCCESS;
#ifdef QE_USING_TICKLESS_LPMGR
    if (0 == strcmp(dev->name, PM_QE_LPMGR_DEV_NAME))
        return OS_SUCCESS;
#endif /* QE_USING_TICKLESS_LPMGR */

    if (OS_NULL == gs_pm_qe_instance)
        return OS_ENOSYS;

    switch (event)
    {
    case ION_GENERIC_OPEN:
        dev_is_open = OS_TRUE;
        break;
    case ION_GENERIC_CLOSE:
        dev_is_open = OS_FALSE;
        break;
    default:
        /* Ignore other status. */
        return OS_SUCCESS;
    }

    /* Nonregistered device */
    if (!pm_qe_find_reg_dev(dev->name))
        return OS_SUCCESS;

    DEBUG("QE CB: device name[%.*s]", OS_NAME_MAX, dev->name);

    return pm_qe_info_find_and_update(dev, dev_is_open, OS_FALSE);
}

#ifdef QE_USING_TICKLESS_LPMGR
void pm_qe_lpmgr_cb(os_lpmgr_sys_e event, lpmgr_sleep_mode_e mode, void *data)
{
    os_bool_t dev_is_open = OS_FALSE;

    if (OS_NULL == gs_pm_qe_instance)
        return;

    switch (event)
    {
    case SYS_ENTER_SLEEP:
        dev_is_open = OS_TRUE;
        break;
    case SYS_EXIT_SLEEP:
        dev_is_open = OS_FALSE;
        break;
    default:
        /* Ignore other status. */
        return;
    }

    pm_qe_info_find_and_update(&lpmgr_dev, dev_is_open, OS_TRUE);
    return;
}
#endif /* QE_USING_TICKLESS_LPMGR */

static os_err_t pm_qe_dev_register()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

#if 0 /* for ECO system */
    OS_ASSERT(OS_NULL == gs_pm_qe_instance->qe_dev_cb_info);
    
    os_err_t result = OS_SUCCESS;

    gs_pm_qe_instance->qe_dev_cb_info = os_calloc(1, sizeof(pm_qe_dev_cb_t));
    if (OS_NULL == gs_pm_qe_instance->qe_dev_cb_info)
    {
        ERROR("QE register error: no enough memory.");
        return OS_NOMEM;
    }
    
    gs_pm_qe_instance->qe_dev_cb_info->cb = pm_qe_device_cb;
    gs_pm_qe_instance->qe_dev_cb_info->register_event = OS_DEV_OPS_TYPE_OPEN | OS_DEV_OPS_TYPE_CLOSE;
    
    result = os_dev_ops_notify_register(gs_pm_qe_instance->qe_dev_cb_info);
    if (OS_SUCCESS != result)
    {
        ERROR("QE register error: callback register failed.");
        os_free(gs_pm_qe_instance->qe_dev_cb_info);
        gs_pm_qe_instance->qe_dev_cb_info = OS_NULL;
    }
#endif

    os_err_t result = os_device_notify_register(OS_NULL, pm_qe_device_cb, OS_NULL);
    if (OS_SUCCESS != result)
    {
        ERROR("QE register error: callback register failed.");
    }

    return result;
}

static os_err_t pm_qe_dev_unregister()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

#if 0 /* for ECO system */
    OS_ASSERT(OS_NULL != gs_pm_qe_instance->qe_dev_cb_info);

    os_err_t result = OS_SUCCESS;

    result = os_dev_ops_notify_unregister(gs_pm_qe_instance->qe_dev_cb_info);
    OS_ASSERT(OS_SUCCESS == result);

    os_free(gs_pm_qe_instance->qe_dev_cb_info);
    gs_pm_qe_instance->qe_dev_cb_info = OS_NULL;
#endif

    os_err_t result = os_device_notify_unregister(OS_NULL, pm_qe_device_cb, OS_NULL);
    OS_ASSERT(OS_SUCCESS == result);
    return result;
}

#ifdef QE_USING_TICKLESS_LPMGR
static os_err_t pm_qe_lpmgr_info_init()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

    os_err_t          result     = OS_SUCCESS;
    pm_qe_dev_info_t *lpmgr_info = OS_NULL;

    lpmgr_info = os_slist_first_entry_or_null(&gs_pm_qe_instance->qe_slist_head, pm_qe_dev_info_t, info_node);
    OS_ASSERT(OS_NULL == lpmgr_info);

    /* Init session */
    lpmgr_info = os_calloc(1, sizeof(pm_qe_dev_info_t));
    if (OS_NULL == lpmgr_info)
    {
        ERROR("QE init lpmgr info error: no enough memory.");
        result = OS_NOMEM;
    }
    else
    {
        DEBUG("QE init lpmgr info.");
        lpmgr_info->dev             = &lpmgr_dev;
        lpmgr_info->working_current = pm_qe_find_dev_current(lpmgr_dev.name);
        os_slist_add_tail(&gs_pm_qe_instance->qe_slist_head, &lpmgr_info->info_node);
    }

    return result;
}

static os_err_t pm_qe_lpmgr_register()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);
    OS_ASSERT(OS_NULL == gs_pm_qe_instance->qe_lpmgr_cb_info);

    os_err_t result = OS_SUCCESS;

    /* Register to lowpower manager */
    gs_pm_qe_instance->qe_lpmgr_cb_info = os_calloc(1, sizeof(pm_qe_lpmgr_cb_t));
    if (OS_NULL == gs_pm_qe_instance->qe_lpmgr_cb_info)
    {
        ERROR("QE lpmgr register error: no enough memory for qe_lpmgr_cb_info.");
        return OS_NOMEM;
    }

    gs_pm_qe_instance->qe_lpmgr_cb_info->notify = pm_qe_lpmgr_cb;

    result = os_lpmgr_notify_register(gs_pm_qe_instance->qe_lpmgr_cb_info);
    if (OS_SUCCESS != result)
    {
        ERROR("QE lpmgr register error: register failed.");
        os_free(gs_pm_qe_instance->qe_lpmgr_cb_info);
        gs_pm_qe_instance->qe_lpmgr_cb_info = OS_NULL;
        return OS_FAILURE;
    }

    return result;
}

static os_err_t pm_qe_lpmgr_unregister()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);
    OS_ASSERT(OS_NULL != gs_pm_qe_instance->qe_lpmgr_cb_info);

    os_err_t result = OS_SUCCESS;

    result = os_lpmgr_notify_unregister(gs_pm_qe_instance->qe_lpmgr_cb_info);
    OS_ASSERT(OS_SUCCESS == result);

    os_free(gs_pm_qe_instance->qe_lpmgr_cb_info);
    gs_pm_qe_instance->qe_lpmgr_cb_info = OS_NULL;

    return result;
}
#endif /* QE_USING_TICKLESS_LPMGR */

static os_bool_t pm_qe_find_reg_dev(char *dev_name)
{
    OS_ASSERT(0 < gs_pm_qe_dev_list_size);

    for (int index = 0; index < gs_pm_qe_dev_list_size; index++)
    {
        if (0 == strcmp(dev_name, pm_qe_dev_list[index].name))
        {
            return OS_TRUE;
        }
    }

    return OS_FALSE;
}

static uint32_t pm_qe_find_dev_current(char *dev_name)
{
    OS_ASSERT(0 < gs_pm_qe_dev_list_size);
    uint32_t working_current = 0;

    for (int index = 0; index < gs_pm_qe_dev_list_size; index++)
    {
        if (0 == strcmp(dev_name, pm_qe_dev_list[index].name))
        {
            OS_ASSERT_EX(0 != pm_qe_dev_list[index].working_current, "please set working_current nonzero");

            working_current = pm_qe_dev_list[index].working_current;
        }
    }

    DEBUG("QE found dev[%.*s] current[%d]uA", OS_NAME_MAX, dev_name, working_current);
    return working_current;
}

static pm_qe_dev_info_t *pm_qe_info_find(os_device_t *dev)
{
    OS_ASSERT(OS_NULL != dev);
    OS_ASSERT(OS_NULL != dev->name);

    pm_qe_dev_info_t *entry = OS_NULL;
    pm_qe_dev_info_t *info  = OS_NULL;
    os_slist_node_t  *node  = OS_NULL;

    os_slist_for_each(node, &gs_pm_qe_instance->qe_slist_head)
    {
        entry = os_slist_entry(node, pm_qe_dev_info_t, info_node);
        if (0 == strcmp(entry->dev->name, dev->name))
            info = entry;
    }
    return info;
}

static void pm_qe_info_release()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

    pm_qe_dev_info_t *entry     = OS_NULL;
    os_slist_node_t  *node      = OS_NULL;
    os_slist_node_t  *node_safe = OS_NULL;

    os_slist_for_each_safe(node, node_safe, &gs_pm_qe_instance->qe_slist_head)
    {
        entry = os_slist_entry(node, pm_qe_dev_info_t, info_node);
        os_free(entry);
    }
}

static void pm_qe_info_update(pm_qe_dev_info_t *info, os_bool_t dev_is_open, os_bool_t is_lpm)
{
    OS_ASSERT(OS_NULL != info);

    /* TODO: Lpm time compensation. */
    if (dev_is_open == info->dev_is_open)
    {
        /* Triggered by same status, ignore it. */
        if (!is_lpm)
            DEBUG("QE info update: same status, ignore it.");
        return;
    }

#ifdef QE_USING_TICKLESS_LPMGR
    if (is_lpm)
    {
        if (dev_is_open)
        {
            gs_pm_qe_instance->qe_lpm_latest_open_time = os_tick_get_value();
        }
        else
        {
            os_tick_t lpm_duration   = 0;
            os_tick_t lpm_close_time = os_tick_get_value();

            if (lpm_close_time < gs_pm_qe_instance->qe_lpm_latest_open_time)
            {
                lpm_duration = lpm_close_time + ((os_tick_t)-1) - gs_pm_qe_instance->qe_lpm_latest_open_time;
            }
            else
            {
                lpm_duration = lpm_close_time - gs_pm_qe_instance->qe_lpm_latest_open_time;
            }

            info->previous_run_time += lpm_duration;
            gs_pm_qe_instance->qe_lpm_latest_open_time = 0;

            qe_lpm_deduction(lpm_duration);
        }
    }
    else
#endif /* QE_USING_TICKLESS_LPMGR */
    {
        if (dev_is_open)
        {
            info->latest_open_time = rtc_get();
        }
        else
        {
            info->previous_run_time += rtc_get() - info->latest_open_time;
            info->latest_open_time = 0;
        }
    }

    info->dev_is_open = dev_is_open;
    if (!is_lpm)
        DEBUG("QE info update: dev[%.*s] go status-%s", OS_NAME_MAX, info->dev->name, dev_is_open ? "open" : "close");
    if (!is_lpm)
        DEBUG("QE info update: previous_run_time[%u], latest_open_time[%u]",
              info->previous_run_time,
              info->latest_open_time);

    return;
}

#ifdef QE_USING_TICKLESS_LPMGR
static void qe_lpm_deduction(os_tick_t lpm_duration)
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);
    OS_ASSERT(OS_NULL != dev);

    pm_qe_dev_info_t *entry = OS_NULL;
    os_slist_node_t  *node  = OS_NULL;

    os_slist_for_each(node, &gs_pm_qe_instance->qe_slist_head)
    {
        entry = os_slist_entry(node, pm_qe_dev_info_t, info_node);
        if (0 == strcmp(entry->dev->name, PM_QE_LPMGR_DEV_NAME))
            continue;
        entry->lpm_deduction += lpm_duration;
    }
}
#endif /* QE_USING_TICKLESS_LPMGR */

static os_err_t pm_qe_info_node_add(os_device_t *dev, os_bool_t dev_is_open)
{
    OS_ASSERT(OS_NULL != dev);
    OS_ASSERT(OS_NULL != dev->name);

    pm_qe_dev_info_t *info = os_calloc(1, sizeof(pm_qe_dev_info_t));
    if (OS_NULL == info)
    {
        ERROR("QE info node add: no enough memory.");
        return OS_NOMEM;
    }

    if (dev_is_open)
    {
        /* Normal */
        info->latest_open_time = rtc_get();
    }
    else
    {
        /* Woops! this device was left out when open. */
        /* Ignore it (call close when dev_close). */
    }

    info->dev             = dev;
    info->working_current = pm_qe_find_dev_current(dev->name);
    info->dev_is_open     = dev_is_open;

    os_slist_add_tail(&gs_pm_qe_instance->qe_slist_head, &info->info_node);

    return OS_SUCCESS;
}

static os_err_t pm_qe_info_find_and_update(os_device_t *dev, os_bool_t dev_is_open, os_bool_t is_lpm)
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

    os_err_t          result = OS_SUCCESS;
    pm_qe_dev_info_t *info   = OS_NULL;

    if (!is_lpm)
        pm_qe_lock(gs_pm_qe_instance->qe_mutex);

    info = pm_qe_info_find(dev);

    if (OS_NULL != info)
    {
        pm_qe_info_update(info, dev_is_open, is_lpm);
    }
    else
    {
        result = pm_qe_info_node_add(dev, dev_is_open);
    }

    if (!is_lpm)
        pm_qe_unlock(gs_pm_qe_instance->qe_mutex);

    return result;
}

static os_err_t pm_qe_param_set(uint32_t capacity, uint32_t percentage)
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);

    if (0 == capacity || 100 < percentage)
    {
        ERROR("QE param set error: invalid param.");
        return OS_INVAL;
    }

    gs_pm_qe_instance->capacity   = capacity;
    gs_pm_qe_instance->percentage = percentage;

    return OS_SUCCESS;
}

os_err_t pm_quantity_estimator_on(uint32_t capacity, uint32_t low_battery_percentage)
{
    if (OS_NULL != gs_pm_qe_instance)
    {
        ERROR("PM Quanty estimator had already been started.");
        return OS_FAILURE;
    }

    os_err_t result = OS_SUCCESS;

    gs_pm_qe_instance = os_calloc(1, sizeof(pm_qe_t));
    if (OS_NULL == gs_pm_qe_instance)
    {
        ERROR("QE init error: no enough memory for instance.");
        return OS_NOMEM;
    }

    result = pm_qe_param_set(capacity, low_battery_percentage);
    if (OS_SUCCESS != result)
    {
        os_free(gs_pm_qe_instance);
        gs_pm_qe_instance = OS_NULL;
        return result;
    }

    gs_pm_qe_instance->qe_mutex = os_mutex_create(NULL, PM_QE_TAG, OS_FALSE);
    if (OS_NULL == gs_pm_qe_instance->qe_mutex)
    {
        ERROR("%s mutex create failed.", PM_QE_TAG);
        os_free(gs_pm_qe_instance);
        gs_pm_qe_instance = OS_NULL;
        return OS_FAILURE;
    }

#ifdef QE_USING_TICKLESS_LPMGR
    result = pm_qe_lpmgr_info_init();
    if (OS_SUCCESS != result)
    {
        os_mutex_destroy(gs_pm_qe_instance->qe_mutex);
        os_free(gs_pm_qe_instance);
        gs_pm_qe_instance = OS_NULL;
        return result;
    }

    result = pm_qe_lpmgr_register();
    if (OS_SUCCESS != result)
    {
        pm_qe_info_release();
        os_mutex_destroy(gs_pm_qe_instance->qe_mutex);
        os_free(gs_pm_qe_instance);
        gs_pm_qe_instance = OS_NULL;
        return result;
    }
#endif /* QE_USING_TICKLESS_LPMGR */

    result = pm_qe_dev_register();
    if (OS_SUCCESS != result)
    {
        pm_qe_info_release();
#ifdef QE_USING_TICKLESS_LPMGR
        pm_qe_lpmgr_unregister();
#endif /* QE_USING_TICKLESS_LPMGR */
        os_mutex_destroy(gs_pm_qe_instance->qe_mutex);
        os_free(gs_pm_qe_instance);
        gs_pm_qe_instance = OS_NULL;
        return result;
    }

    gs_pm_qe_instance->qe_start_time = rtc_get();
    DEBUG("%s-%d: qe_start_time[%u]", __func__, __LINE__, gs_pm_qe_instance->qe_start_time);

    return result;
}

os_err_t pm_quantity_estimator_off()
{
    if (OS_NULL == gs_pm_qe_instance)
    {
        ERROR("PM Quanty estimator had already been stopped.");
        return OS_FAILURE;
    }

    pm_qe_info_release();
    pm_qe_dev_unregister();
#ifdef QE_USING_TICKLESS_LPMGR
    pm_qe_lpmgr_unregister();
#endif /* QE_USING_TICKLESS_LPMGR */
    os_mutex_destroy(gs_pm_qe_instance->qe_mutex);
    os_free(gs_pm_qe_instance);
    gs_pm_qe_instance = OS_NULL;

    return OS_SUCCESS;
}

os_bool_t pm_quantity_estimator_is_on()
{
    return (OS_NULL != gs_pm_qe_instance) ? OS_TRUE : OS_FALSE;
}

os_err_t pm_qe_reset(uint32_t capacity, uint32_t low_battery_percentage)
{
    pm_qe_dev_info_t *entry = OS_NULL;
    os_slist_node_t  *node  = OS_NULL;

    if (OS_NULL == gs_pm_qe_instance)
    {
        ERROR("Quantity estimator is off, cannot reset.");
        return OS_FAILURE;
    }

    pm_qe_lock(gs_pm_qe_instance->qe_mutex);

    if (OS_SUCCESS != pm_qe_param_set(capacity, low_battery_percentage))
    {
        pm_qe_unlock(gs_pm_qe_instance->qe_mutex);
        return OS_INVAL;
    }

    os_slist_for_each(node, &gs_pm_qe_instance->qe_slist_head)
    {
        entry = os_slist_entry(node, pm_qe_dev_info_t, info_node);

#ifdef QE_USING_TICKLESS_LPMGR
        if (0 == strcmp(PM_QE_LPMGR_DEV_NAME, entry->dev->name))
        {
            gs_pm_qe_instance->qe_lpm_latest_open_time = (entry->dev_is_open) ? os_tick_get_value() : 0;
            entry->lpm_deduction                       = 0;
        }
        else
#endif /* QE_USING_TICKLESS_LPMGR */
        {
            entry->previous_run_time = 0;
            entry->latest_open_time  = (entry->dev_is_open) ? rtc_get() : 0;
        }
    }

    gs_pm_qe_instance->qe_start_time = rtc_get();
    DEBUG("%s-%d: qe_start_time[%u]", __func__, __LINE__, gs_pm_qe_instance->qe_start_time);

    pm_qe_unlock(gs_pm_qe_instance->qe_mutex);

    return OS_SUCCESS;
}

static float pm_qe_calc_power_consumption(pm_qe_dev_info_t *info, time_t calc_moment, time_t lpm_deduction)
{
    OS_ASSERT(OS_NULL != info);
    time_t duration    = 0;
    float  consumption = 0;
    float  current     = info->working_current;

    duration = info->previous_run_time + (info->dev_is_open ? calc_moment - info->latest_open_time : 0) - lpm_deduction;
    consumption = duration * current / (PM_QE_SEC_PER_HOUR * 1000);

    DEBUG("QE dev[%s] duration:%us, current:%duA, comsumption:%f mAh",
          info->dev->name,
          duration,
          info->working_current,
          consumption);
    return consumption;
}

int32_t pm_qe_used_get()
{
    OS_ASSERT_EX(OS_NULL != gs_pm_qe_instance, "PM QE %s-%d: not allowed, quantity estimator is off!");

    time_t     normal_duration   = 0;
    time_t     calc_moment       = 0;
    float      calc_consumption  = 0;
    float      current           = 0;
    int32_t    power_consumption = 0;
    time_t     lpmgr_duration    = 0;

#ifdef QE_USING_TICKLESS_LPMGR
    time_t            lpm_time         = 0;
    time_t            lpm_duration_cur = 0;
    pm_qe_dev_info_t *info             = OS_NULL;
#endif /* QE_USING_TICKLESS_LPMGR */

    pm_qe_dev_info_t *entry = OS_NULL;
    os_slist_node_t  *node  = OS_NULL;

    pm_qe_lock(gs_pm_qe_instance->qe_mutex);

    calc_moment = rtc_get();
    DEBUG("%s-%d: trigger moment[%u]", __func__, __LINE__, calc_moment);

    /* Calc normal work status power_consumption */
#ifdef QE_USING_TICKLESS_LPMGR
    info     = os_slist_first_entry_or_null(&gs_pm_qe_instance->qe_slist_head, pm_qe_dev_info_t, info_node);
    lpm_time = os_tick_get_value();

    OS_ASSERT(OS_NULL != info);
    OS_ASSERT(!info->dev_is_open);

    lpmgr_duration   = info->previous_run_time / OS_TICK_PER_SECOND;
    current          = pm_qe_find_dev_current(PM_QE_LPMGR_DEV_NAME);
    calc_consumption = lpmgr_duration * current / (PM_QE_SEC_PER_HOUR * 1000);
    DEBUG("QE dev[%s] duration:%us, comsumption:%f mAh", PM_QE_LPMGR_DEV_NAME, lpmgr_duration, calc_consumption);
#endif /* QE_USING_TICKLESS_LPMGR */

    normal_duration = calc_moment - gs_pm_qe_instance->qe_start_time - lpmgr_duration;
    current         = pm_qe_find_dev_current(PM_QE_NORMAL_DEV_NAME);
    calc_consumption += normal_duration * current / (PM_QE_SEC_PER_HOUR * 1000);
    DEBUG("QE dev[normal] duration:%us, add up comsumption:%f mAh", normal_duration, calc_consumption);

    /* Calc all dev work status power_consumption */
    os_slist_for_each(node, &gs_pm_qe_instance->qe_slist_head)
    {
        entry = os_slist_entry(node, pm_qe_dev_info_t, info_node);

#ifdef QE_USING_TICKLESS_LPMGR
        /* Skip the lpmgr which is already add up. */
        if (0 == strcmp(PM_QE_LPMGR_DEV_NAME, entry->dev->name))
            continue;
#endif /* QE_USING_TICKLESS_LPMGR */

        calc_consumption += pm_qe_calc_power_consumption(entry, calc_moment, lpmgr_duration);
    }

    power_consumption = calc_consumption;

    pm_qe_unlock(gs_pm_qe_instance->qe_mutex);

    DEBUG("%s-%d: get power consumption[%d]", __func__, __LINE__, power_consumption);
    return power_consumption;
}

int32_t pm_qe_left_get(uint32_t unstated_using)
{
    OS_ASSERT_EX(OS_NULL != gs_pm_qe_instance, "PM QE %s-%d: not allowed, quantity estimator is off!");

    int32_t battery_used      = 0;
    int32_t avaliable_battery = 0;

    /* Approximate */
    battery_used      = pm_qe_used_get();
    avaliable_battery = gs_pm_qe_instance->capacity * (100 - gs_pm_qe_instance->percentage) / 100;

    return (0 > battery_used) ? battery_used : (avaliable_battery - battery_used - unstated_using);
}

os_bool_t pm_qe_is_low(uint32_t unstated_using)
{
    /* Do not call it, when quantity estimator off */
    OS_ASSERT_EX(OS_NULL != gs_pm_qe_instance, "PM QE %s-%d: not allowed, quantity estimator is off!");

    return (0 <= pm_qe_left_get(unstated_using)) ? OS_FALSE : OS_TRUE;
}

#ifdef PM_QE_AUTO_CREATE
os_err_t pm_qe_auto_create()
{
    return pm_quantity_estimator_on(PM_QE_BATTERY_CAPACITY_DFT, PM_QE_LOW_BATTERY_PERCENTAGE_DFT);
}
OS_INIT_CALL(pm_qe_auto_create, OS_INIT_LEVEL_PRE_KERNEL_1, OS_INIT_SUBLEVEL_LOW);

os_err_t pm_qe_rtc_set()
{
    OS_ASSERT(OS_NULL != gs_pm_qe_instance);
    pm_qe_dev_info_t *entry = OS_NULL;
    os_slist_node_t  *node  = OS_NULL;

    /* RTC init after QM update newest timestamp */
    gs_pm_qe_instance->qe_start_time = rtc_get();
    DEBUG("%s-%d: qe_start_time[%u]", __func__, __LINE__, gs_pm_qe_instance->qe_start_time);

    os_slist_for_each(node, &gs_pm_qe_instance->qe_slist_head)
    {
        entry                    = os_slist_entry(node, pm_qe_dev_info_t, info_node);
        entry->previous_run_time = 0;

#ifdef QE_USING_TICKLESS_LPMGR
        if (0 == strcmp(PM_QE_LPMGR_DEV_NAME, entry->dev->name))
            continue;
#endif /* QE_USING_TICKLESS_LPMGR */

        if (entry->dev_is_open)
        {
            entry->latest_open_time = gs_pm_qe_instance->qe_start_time;
        }
    }

    return OS_SUCCESS;
}

OS_INIT_CALL(pm_qe_rtc_set, OS_INIT_LEVEL_APPLICATION, OS_INIT_SUBLEVEL_LOW);
#endif /* PM_QE_AUTO_CREATE */
