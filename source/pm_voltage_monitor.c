/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_voltage_monitor.c
 *
 * @brief       Implements of voltage monitor
 *
 * @revision
 * Date         Author          Notes
 * 2021-08-9    OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <oneos_config.h>
#include <pm_voltage_monitor.h>
#include <os_assert.h>
#include <os_sem.h>
#include <os_task.h>
#include <os_mutex.h>
#include <os_timer.h>
#include <os_errno.h>
#include <os_memory.h>
#include <drv_cfg.h>
#include <sensors/sensor.h>
#include <string.h>
#include <math.h>
#include <string.h>

#ifndef PM_LOG_LVL
#define PM_LOG_LVL PM_VM_LOG_LVL
#endif /* PM_LOG_LVL */
#include <pm_log.h>

#define PM_VM_EPSILON        (1e-5)
#define PM_VM_TAG            "PM_VM"
#define PM_VM_DATA_SIZE      (sizeof(int32_t))
#define PM_VM_DATA_INFO_SIZE (sizeof(data_info_t))

#ifdef PM_VM_USING_FAL
#define PM_VM_FAL_TAG "PM_VM_FAL"
#endif /* PM_VM_USING_FAL */

#define PM_VM_SAMPLING_DURATION (PM_VM_SAMPLING_DURATION_SEC * OS_TICK_PER_SECOND)
#define PM_VM_TASK_PRIORITY     (0x0A)

#define PM_IS_NULL_RETURN(ret, str, errno)                                                                             \
    {                                                                                                                  \
        if (OS_NULL == ret)                                                                                            \
        {                                                                                                              \
            ERROR(str);                                                                                                \
            return errno;                                                                                              \
        }                                                                                                              \
    }
#define PM_IS_NOT_NULL_RETURN(ret, str, errno)                                                                         \
    {                                                                                                                  \
        if (OS_NULL != ret)                                                                                            \
        {                                                                                                              \
            ERROR(str);                                                                                                \
            return errno;                                                                                              \
        }                                                                                                              \
    }
#define PM_IS_ERRNO_RETURN(ret, str)                                                                                   \
    {                                                                                                                  \
        os_err_t __ret = ret;                                                                                          \
        if (OS_SUCCESS != __ret)                                                                                       \
        {                                                                                                              \
            ERROR(str "ERRNO:%d", __ret);                                                                              \
            return __ret;                                                                                              \
        }                                                                                                              \
    }

#ifdef PM_VM_USING_PINCTL
void pm_vm_pinctl_adc_enable(void);
void pm_vm_pinctl_adc_disable(void);
#endif /* PM_VM_USING_PINCTL */
struct pm_vm_data_info
{
    uint32_t check_num;
    uint32_t head_pos;
    uint32_t data_cnt;
} __attribute__((aligned(4)));

typedef struct pm_vm_data_info data_info_t;

typedef struct pm_vm
{
    os_task_id      pm_vm_task;
    os_mutex_id     pm_vm_lock;
    os_semaphore_id pm_vm_sem;
    os_timer_id     pm_vm_timer;
    os_device_t    *pm_vm_adc_dev;
    os_device_t    *pm_vm_temp_dev;
    data_info_t    *pm_vm_data_info;
    uint32_t        pm_vm_info_size;
    int32_t        *pm_vm_data_buff;
    uint32_t        pm_vm_data_size;
#ifdef PM_VM_USING_FAL
    os_mutex_id pm_vm_fal_lock;
    fal_part_t *pm_vm_fal_part;
#endif                             /* PM_VM_USING_FAL */
    int32_t     voltage_threshold; /* voltage threshold with millivolt */
    pm_vm_cfg_t optional_cfg;
} pm_vm_t;

typedef os_err_t (*pm_vm_func_t)(void);

typedef enum pm_vm_init_state
{
    PM_VM_INSTANCE_CREATE = 0,
    PM_VM_MUTEX_CREATE,
    PM_VM_CFG_INIT,
    PM_VM_TIMER_CREATE,
#ifdef PM_VM_USING_FAL
    PM_VM_FAL_INIT,
#endif /* PM_VM_USING_FAL */
    PM_VM_TASK_CREATE,
    PM_VM_TIMER_START,
    PM_VM_INIT_FINISHED,
} pm_vm_init_state_t;

typedef struct pm_vm_act_map
{
    pm_vm_init_state_t state;
    pm_vm_func_t       do_func;
    pm_vm_func_t       err_func;
} pm_vm_act_map_t;

/* Sigle instance */
static pm_vm_t *gs_pm_vm_instance = OS_NULL;

void pm_vm_lock(os_mutex_id mutex)
{
    os_mutex_lock(mutex, OS_WAIT_FOREVER);
}

void pm_vm_unlock(os_mutex_id mutex)
{
    os_mutex_unlock(mutex);
}

os_err_t pm_vm_mutex_create()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);

    gs_pm_vm_instance->pm_vm_lock = os_mutex_create(OS_NULL, PM_VM_TAG, OS_FALSE);
    if (OS_NULL == gs_pm_vm_instance->pm_vm_lock)
    {
        ERROR("%s mutex create failed.", PM_VM_TAG);
        return OS_FAILURE;
    }

#ifdef PM_VM_USING_FAL
    gs_pm_vm_instance->pm_vm_fal_lock = os_mutex_create(OS_NULL, PM_VM_FAL_TAG, OS_FALSE);
    if (OS_NULL == gs_pm_vm_instance->pm_vm_fal_lock)
    {
        os_mutex_destroy(gs_pm_vm_instance->pm_vm_lock);
        ERROR("%s mutex create failed.", PM_VM_TAG);
        return OS_FAILURE;
    }
#endif /* PM_VM_USING_FAL */

    return OS_SUCCESS;
}

os_err_t pm_vm_mutex_destroy()
{
    os_err_t result = OS_SUCCESS;

    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_lock);

    result = os_mutex_destroy(gs_pm_vm_instance->pm_vm_lock);
    OS_ASSERT(OS_SUCCESS == result);

#ifdef PM_VM_USING_FAL
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_fal_lock);

    result = os_mutex_destroy(gs_pm_vm_instance->pm_vm_fal_lock);
    OS_ASSERT(OS_SUCCESS == result);
#endif
    return result;
}

os_err_t pm_vm_instance_create()
{
    PM_IS_NOT_NULL_RETURN(gs_pm_vm_instance, "The instance has already started.", OS_FAILURE);

    gs_pm_vm_instance = os_calloc(1, sizeof(pm_vm_t));
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Instance start failed, no enough memory for instance.", OS_NOMEM);

#ifdef PM_VM_USING_FAL
    fal_part_t *fal_part = fal_part_find(PM_VM_FAL_NAME);
    PM_IS_NULL_RETURN(fal_part, "FAL part name not found.", OS_INVAL);

    uint32_t pm_vm_fal_align           = fal_part_page_size(fal_part);
    gs_pm_vm_instance->pm_vm_info_size = OS_ALIGN_UP(PM_VM_DATA_INFO_SIZE, pm_vm_fal_align);
    gs_pm_vm_instance->pm_vm_data_size = OS_ALIGN_UP(PM_VM_DATA_SIZE * PM_VM_DATA_MAX, pm_vm_fal_align);
#else
    gs_pm_vm_instance->pm_vm_info_size = PM_VM_DATA_INFO_SIZE;
    gs_pm_vm_instance->pm_vm_data_size = PM_VM_DATA_SIZE * PM_VM_DATA_MAX;
#endif /* PM_VM_USING_FAL */

    gs_pm_vm_instance->pm_vm_data_buff = os_calloc(1, gs_pm_vm_instance->pm_vm_data_size);
    if (OS_NULL == gs_pm_vm_instance->pm_vm_data_buff)
    {
        os_free(gs_pm_vm_instance);
        gs_pm_vm_instance = OS_NULL;
        ERROR("Instance start failed, no enough memory for data buff.");
        return OS_NOMEM;
    }

    gs_pm_vm_instance->pm_vm_data_info = os_calloc(1, gs_pm_vm_instance->pm_vm_info_size);
    if (OS_NULL == gs_pm_vm_instance->pm_vm_data_buff)
    {
        os_free(gs_pm_vm_instance->pm_vm_data_buff);
        os_free(gs_pm_vm_instance);
        gs_pm_vm_instance = OS_NULL;
        ERROR("Instance start failed, no enough memory for data info.");
        return OS_NOMEM;
    }

    return OS_SUCCESS;
}

os_err_t pm_vm_instance_destroy()
{
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Instance had already destroyed.", OS_FAILURE);

    os_free(gs_pm_vm_instance->pm_vm_data_info);
    os_free(gs_pm_vm_instance->pm_vm_data_buff);
    os_free(gs_pm_vm_instance);
    gs_pm_vm_instance = OS_NULL;
    return OS_SUCCESS;
}

os_err_t pm_vm_set_voltage_threshold(int32_t threshold_mv)
{
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Voltage monitor has not been initialized.", OS_FAILURE);
    if (0 >= threshold_mv)
    {
        ERROR("%s input invalid threshold.", __func__);
        return OS_INVAL;
    }

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    gs_pm_vm_instance->voltage_threshold = threshold_mv;

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_set_calibration_coefficient(float calibration_coefficient)
{
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Voltage monitor has not been initialized.", OS_FAILURE);

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    gs_pm_vm_instance->optional_cfg.calibration_coefficient = calibration_coefficient;

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_set_calibration_base(int32_t calibration_base)
{
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Voltage monitor has not been initialized.", OS_FAILURE);

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    gs_pm_vm_instance->optional_cfg.calibration_base = calibration_base;

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_set_temp_correction_cb(pm_vm_temp_cb_t temp_correction_cb)
{
    PM_IS_NULL_RETURN(gs_pm_vm_instance, "Voltage monitor has not been initialized.", OS_FAILURE);

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    gs_pm_vm_instance->optional_cfg.temp_correction_cb = temp_correction_cb;
    gs_pm_vm_instance->pm_vm_temp_dev =
        (OS_NULL == temp_correction_cb) ? OS_NULL : os_device_find(PM_VM_ONCHIP_TEMP_SENSOR_NAME);

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);

    return ((OS_NULL != temp_correction_cb) && (OS_NULL == gs_pm_vm_instance->pm_vm_temp_dev)) ? OS_FAILURE
                                                                                               : OS_SUCCESS;
}

os_err_t pm_vm_cfg_init(int32_t threshold_mv, pm_vm_cfg_t *optional_cfg)
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);

    if (0 >= threshold_mv)
    {
        ERROR("Input invalid voltage threshold.");
        return OS_INVAL;
    }

    gs_pm_vm_instance->voltage_threshold = threshold_mv;

    if (OS_NULL == optional_cfg)
    {
        DEBUG("No optional config.");
    }
    else
    {
        gs_pm_vm_instance->pm_vm_temp_dev = os_device_find(PM_VM_ONCHIP_TEMP_SENSOR_NAME);
        if (OS_NULL != optional_cfg->temp_correction_cb)
        {
            if (OS_NULL == gs_pm_vm_instance->pm_vm_temp_dev)
            {
                ERROR("Input invalid temperature sensor name.");
                return OS_INVAL;
            }
        }
        else
        {
            gs_pm_vm_instance->pm_vm_temp_dev = OS_NULL;
        }

        gs_pm_vm_instance->pm_vm_adc_dev = os_device_find(PM_VM_BATTERY_ADC_DEVICE_NAME);
        if ((0 > PM_VM_BATTERY_ADC_CHANNEL) || (OS_NULL == gs_pm_vm_instance->pm_vm_adc_dev))
        {
            ERROR("Input invalid battery adc channel/name.");
            return OS_INVAL;
        }

        memcpy(&gs_pm_vm_instance->optional_cfg, optional_cfg, sizeof(pm_vm_cfg_t));
    }

    return OS_SUCCESS;
}

void pm_vm_timer_cb(void *parameter)
{
    DEBUG("%s-%d", __func__, __LINE__);

    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_sem);

    if (OS_SUCCESS != os_semaphore_post(gs_pm_vm_instance->pm_vm_sem))
    {
        ERROR("%s-%d:sem post failed.", __func__, __LINE__);
    }
}

/* clang-format off */
os_err_t pm_vm_timer_create()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);

    PM_IS_NOT_NULL_RETURN(gs_pm_vm_instance->pm_vm_timer, "Timer has already created.", OS_FAILURE);

    os_tick_t timeout = gs_pm_vm_instance->optional_cfg.sampling_duration_sec * OS_TICK_PER_SECOND;

    if (0 == timeout)
    {
        timeout = PM_VM_SAMPLING_DURATION;
    }

    gs_pm_vm_instance->pm_vm_timer = os_timer_create(NULL,
                                                     PM_VM_TAG,
                                                     pm_vm_timer_cb,
                                                     OS_NULL,
                                                     timeout,
                                                     OS_TIMER_FLAG_PERIODIC);

    PM_IS_NULL_RETURN(gs_pm_vm_instance->pm_vm_timer, "Timer failed to create.", OS_FAILURE);

    return OS_SUCCESS;
}
/* clang-format on */

os_err_t pm_vm_timer_start()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_timer);

    PM_IS_ERRNO_RETURN(os_timer_start(gs_pm_vm_instance->pm_vm_timer), "Timer failed to start.");
    return OS_SUCCESS;
}

os_err_t pm_vm_timer_stop()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_timer);

    PM_IS_ERRNO_RETURN(os_timer_stop(gs_pm_vm_instance->pm_vm_timer), "Timer failed to stop.");
    return OS_SUCCESS;
}

os_err_t pm_vm_timer_destroy()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_timer);

    PM_IS_ERRNO_RETURN(os_timer_destroy(gs_pm_vm_instance->pm_vm_timer), "Timer failed to destroy.");
    gs_pm_vm_instance->pm_vm_timer = OS_NULL;
    return OS_SUCCESS;
}

static int32_t pm_vm_get_latest_data_index(data_info_t *info)
{
    OS_ASSERT(OS_NULL != info);

    uint32_t data_pos   = 0; /* NO. of data */
    uint32_t data_index = 0; /* Array index of pm_vm_data_buff */

    if (PM_VM_DATA_MAX > info->data_cnt)
    {
        data_pos = info->data_cnt;
    }
    else
    {
        /* Means data head is in the first place of fal data part. */
        if (1 == info->head_pos)
        {
            data_pos = PM_VM_DATA_MAX;
        }
        else
        {
            data_pos = info->head_pos - 1;
        }
    }

    /* Array index is number of data - 1 */
    data_index = data_pos - 1;

    return data_index;
}

#ifdef PM_VM_USING_FAL
os_err_t pm_vm_fal_read(uint32_t addr, uint8_t *buf, size_t size)
{
    OS_ASSERT(OS_NULL == gs_pm_vm_instance->pm_vm_fal_part);

    pm_vm_lock(gs_pm_vm_instance->pm_vm_fal_lock);

    if (size != fal_part_read(gs_pm_vm_instance->pm_vm_fal_part, addr, buf, size))
    {
        ERROR("FAL read failed.");
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);
        return OS_EIO;
    }

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_fal_write(uint32_t addr, const uint8_t *buf, size_t size)
{
    pm_vm_lock(gs_pm_vm_instance->pm_vm_fal_lock);

    if (size != fal_part_write(gs_pm_vm_instance->pm_vm_fal_part, addr, buf, size))
    {
        ERROR("FAL write failed!");
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);
        return OS_EIO;
    }

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_fal_clear_history()
{
    pm_vm_lock(gs_pm_vm_instance->pm_vm_fal_lock);

    if (0 > fal_part_erase_all(gs_pm_vm_instance->pm_vm_fal_part))
    {
        ERROR("FAL clear history failed, fal part erase all failed.");
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);
        return OS_FAILURE;
    }

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_fal_part_erase(uint32_t addr, size_t size)
{
    pm_vm_lock(gs_pm_vm_instance->pm_vm_fal_lock);

    if (size != fal_part_erase(gs_pm_vm_instance->pm_vm_fal_part, addr, size))
    {
        ERROR("FAL erase failed!");
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);
        return OS_EIO;
    }

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_fal_lock);

    return OS_SUCCESS;
}

os_err_t pm_vm_fal_data_save()
{
    os_err_t result = OS_SUCCESS;

    result = pm_vm_fal_clear_history();
    PM_IS_ERRNO_RETURN(result, "FAL erase data befor write failed.");

    result = pm_vm_fal_write(gs_pm_vm_instance->pm_vm_info_size,
                             (uint8_t *)gs_pm_vm_instance->pm_vm_data_buff,
                             gs_pm_vm_instance->pm_vm_data_size);
    PM_IS_ERRNO_RETURN(result, "FAL write data failed.");

    result = pm_vm_fal_write(0, (uint8_t *)(gs_pm_vm_instance->pm_vm_data_info), gs_pm_vm_instance->pm_vm_info_size);
    PM_IS_ERRNO_RETURN(result, "FAL write fal info failed.");
    return OS_SUCCESS;
}

os_err_t pm_vm_fal_init()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL == gs_pm_vm_instance->pm_vm_fal_part);

    data_info_t *fal_info  = gs_pm_vm_instance->pm_vm_data_info;
    int32_t      fal_ret   = 0;
    uint32_t     info_size = gs_pm_vm_instance->pm_vm_info_size;
    uint32_t     data_size = gs_pm_vm_instance->pm_vm_data_size;

    gs_pm_vm_instance->pm_vm_fal_part = fal_part_find(PM_VM_FAL_NAME);
    PM_IS_NULL_RETURN(gs_pm_vm_instance->pm_vm_fal_part, "FAL part name not found.", OS_INVAL);

    /* Check fal part maximum size is enough for data storage? */
    fal_ret = info_size + data_size;
    if (fal_part_size(gs_pm_vm_instance->pm_vm_fal_part) < fal_ret)
    {
        ERROR("FAL init failed, fal part size not enough.");
        gs_pm_vm_instance->pm_vm_fal_part = OS_NULL;
        return OS_INVAL;
    }

    /* Read flash info part. */
    fal_ret = fal_part_read(gs_pm_vm_instance->pm_vm_fal_part, 0, (uint8_t *)fal_info, info_size);
    if (info_size != fal_ret)
    {
        ERROR("FAL init failed, read fal info failed.");
        return OS_FAILURE;
    }

    /* Check the validity of flash info. */
    if (fal_info->check_num != (fal_info->head_pos ^ fal_info->data_cnt))
    {
        INFO("FAL erase all. check sum:%u, head_pos:%u, data_cnt:%u",
             fal_info->check_num,
             fal_info->head_pos,
             fal_info->data_cnt);
        pm_vm_fal_clear_history();
        memset(fal_info, 0, PM_VM_DATA_INFO_SIZE);
        return OS_SUCCESS;
    }

    /* Recovery data history from flash. */
    fal_ret = fal_part_read(gs_pm_vm_instance->pm_vm_fal_part,
                            info_size,
                            (uint8_t *)gs_pm_vm_instance->pm_vm_data_buff,
                            data_size);
    if (data_size != fal_ret)
    {
        ERROR("FAL init failed, read data from flash failed.");
        return OS_FAILURE;
    }

    return OS_SUCCESS;
}

static int32_t pm_vm_get_latest_data_from_flash()
{
    fal_part_t *fal_part    = OS_NULL;
    data_info_t fal_info    = {0};
    int32_t     fal_ret     = 0;
    uint32_t    data_index  = 0; /* Data index */
    int32_t     voltage_mv  = 0; /* Latest voltage datum. */
    uint32_t    fal_align   = 0;
    uint32_t    info_offset = 0;

    fal_part = fal_part_find(PM_VM_FAL_NAME);
    PM_IS_NULL_RETURN(fal_part, "FAL part name not found.", OS_INVAL);

    /* Read flash info part. */
    fal_ret = fal_part_read(fal_part, 0, (uint8_t *)&fal_info, PM_VM_DATA_INFO_SIZE);
    if (PM_VM_DATA_INFO_SIZE != fal_ret)
    {
        ERROR("FAL init failed, read fal info failed.");
        return OS_FAILURE;
    }

    /* Check the validity of flash info. */
    if (fal_info.check_num != ((fal_info.head_pos) ^ (fal_info.data_cnt)))
    {
        ERROR("FAL part data invalid. check sum:%u, head_pos:%u, data_cnt:%u",
              fal_info.check_num,
              fal_info.head_pos,
              fal_info.data_cnt);
        return OS_FAILURE;
    }

    /* Caculate the info offset. */
    fal_align   = fal_part_page_size(fal_part);
    info_offset = OS_ALIGN_UP(PM_VM_DATA_INFO_SIZE, fal_align);

    /* Recovery data history from flash. */
    data_index = pm_vm_get_latest_data_index(&fal_info);

    fal_ret =
        fal_part_read(fal_part, info_offset + data_index * PM_VM_DATA_SIZE, (uint8_t *)&voltage_mv, PM_VM_DATA_SIZE);
    if (PM_VM_DATA_SIZE != fal_ret)
    {
        ERROR("FAL get latest data failed, read data from flash failed.");
        return OS_FAILURE;
    }

    DEBUG("%s-%d: latest data from flash[%d]mV", __func__, __LINE__, voltage_mv);

    return voltage_mv;
}

static int32_t pm_vm_get_history_data_from_flash(int32_t *data_array, os_size_t history_count)
{
    fal_part_t *fal_part        = OS_NULL;
    data_info_t fal_info        = {0};
    int32_t     fal_ret         = 0;
    uint32_t    index           = 0;
    uint32_t    fal_align       = 0;
    uint32_t    info_offset     = 0;
    os_size_t   total_write_cnt = 0; /* Total number of data that will be saved to the user array */
    os_size_t   head_write_cnt  = 0;
    os_size_t   tail_write_cnt  = 0;

    fal_part = fal_part_find(PM_VM_FAL_NAME);
    PM_IS_NULL_RETURN(fal_part, "FAL part name not found.", OS_INVAL);

    /* Read flash info part. */
    fal_ret = fal_part_read(fal_part, 0, (uint8_t *)&fal_info, PM_VM_DATA_INFO_SIZE);
    if (PM_VM_DATA_INFO_SIZE != fal_ret)
    {
        ERROR("FAL init failed, read fal info failed.");
        return OS_FAILURE;
    }

    /* Check the validity of flash info. */
    if (fal_info.check_num != ((fal_info.head_pos) ^ (fal_info.data_cnt)))
    {
        ERROR("FAL part data invalid. check sum:%u, head_pos:%u, data_cnt:%u",
              fal_info.check_num,
              fal_info.head_pos,
              fal_info.data_cnt);
        return OS_FAILURE;
    }

    if (0 == fal_info.data_cnt)
    {
        ERROR("Get history data failed, no history data.");
        return OS_EMPTY;
    }

    /* Caculate the info offset. */
    fal_align   = fal_part_page_size(fal_part);
    info_offset = OS_ALIGN_UP(PM_VM_DATA_INFO_SIZE, fal_align);

    /* Recovery data history from flash. */
    total_write_cnt = (history_count <= fal_info.data_cnt) ? history_count : fal_info.data_cnt;

    /* Copy the head_part, if the user need data all in head part. */
    if (total_write_cnt <= (fal_info.head_pos - 1))
    {
        index = fal_info.head_pos - total_write_cnt - 1;

        fal_ret = fal_part_read(fal_part,
                                info_offset + PM_VM_DATA_SIZE * index,
                                (uint8_t *)data_array,
                                PM_VM_DATA_SIZE * total_write_cnt);
        if (PM_VM_DATA_SIZE * total_write_cnt != fal_ret)
        {
            ERROR("FAL read failed, read data from flash failed.");
            return OS_FAILURE;
        }
    }
    else
    {
        /**
         * Because the data in memory is discontinuous, to get the latest
         * history_count number of data, we devide it into two parts.
         * 1. find & copy the head part.
         * 2. find & copy the tail part.
         *
         * sketch map：
         *  O O O O  O O O O  O O O O  O O O O O
         *  - - - -  - - - -  - - - -  - - - - -
         *  |------------| ↑
         *         ↑       |-------------------|
         *    head_part    |          ↑
         *             [head_pos]     |
         *                       tail_part
         */

        head_write_cnt = fal_info.head_pos - 1;
        tail_write_cnt = total_write_cnt - head_write_cnt;

        fal_ret = fal_part_read(fal_part,
                                info_offset,
                                (uint8_t *)(data_array + tail_write_cnt),
                                PM_VM_DATA_SIZE * head_write_cnt);
        fal_ret += fal_part_read(fal_part,
                                 info_offset + (fal_info.data_cnt - tail_write_cnt) * PM_VM_DATA_SIZE,
                                 (uint8_t *)data_array,
                                 PM_VM_DATA_SIZE * tail_write_cnt);

        if (PM_VM_DATA_SIZE * total_write_cnt != fal_ret)
        {
            ERROR("FAL read failed, read data from flash failed.");
            return OS_FAILURE;
        }
    }

    return total_write_cnt;
}
#endif /* PM_VM_USING_FAL */

int32_t pm_vm_get_latest_data()
{
    if (OS_NULL == gs_pm_vm_instance || OS_NULL == gs_pm_vm_instance->pm_vm_lock)
    {
#ifdef PM_VM_USING_FAL
        INFO("Power monitor has not been initialized, try get data from flash.");
        return pm_vm_get_latest_data_from_flash();
#else
        INFO("Power monitor has not been initialized, get latest data error.");
        return OS_FAILURE;
#endif /* PM_VM_USING_FAL */
    }

    uint32_t data_index = 0; /* Array index of pm_vm_data_buff */
    int32_t  voltage_mv = 0; /* Latest voltage datum. */

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    data_index = pm_vm_get_latest_data_index(gs_pm_vm_instance->pm_vm_data_info);

    voltage_mv = gs_pm_vm_instance->pm_vm_data_buff[data_index];

    DEBUG("%s-%d: latest data from mem[%d]mV", __func__, __LINE__, voltage_mv);

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);

    return voltage_mv;
}

int32_t pm_vm_get_history_data(int32_t *data_array, os_size_t history_count)
{
    if (OS_NULL == gs_pm_vm_instance || OS_NULL == gs_pm_vm_instance->pm_vm_lock)
    {
#ifdef PM_VM_USING_FAL
        INFO("Power monitor has not been initialized, try get data from flash.");
        return pm_vm_get_history_data_from_flash(data_array, history_count);
#else
        INFO("Power monitor has not been initialized, get history error.");
        return OS_FAILURE;
#endif /* PM_VM_USING_FAL */
    }

    data_info_t *data_info       = gs_pm_vm_instance->pm_vm_data_info;
    uint32_t     index           = 0;
    os_size_t    total_write_cnt = 0; /* Total number of data that will be saved to the user array */
    os_size_t    head_write_cnt  = 0;
    os_size_t    tail_write_cnt  = 0;

    /**
     * Because the data in memory is discontinuous, to get the latest
     * history_count number of data, we devide it into two parts.
     * 1. find & copy the head part.
     * 2. find & copy the tail part.
     *
     * sketch map：
     *  O O O O  O O O O  O O O O  O O O O O
     *  - - - -  - - - -  - - - -  - - - - -
     *  |------------| ↑
     *         ↑       |-------------------|
     *    head_part    |          ↑
     *             [head_pos]     |
     *                       tail_part
     */

    if (OS_NULL == data_array || 0 == history_count)
    {
        ERROR("Get history: data input invalid.");
        return OS_INVAL;
    }

    pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);

    if (0 == data_info->data_cnt)
    {
        ERROR("Get history data failed, no history data.");
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);
        return OS_EMPTY;
    }

    total_write_cnt = (history_count <= data_info->data_cnt) ? history_count : data_info->data_cnt;

    /* Copy the head_part, if the user need data all in head part. */
    if (total_write_cnt <= (data_info->head_pos - 1))
    {
        index = data_info->head_pos - total_write_cnt - 1;
        memcpy(data_array, gs_pm_vm_instance->pm_vm_data_buff + index, PM_VM_DATA_SIZE * total_write_cnt);
    }
    else
    {
        head_write_cnt = data_info->head_pos - 1;
        tail_write_cnt = total_write_cnt - head_write_cnt;
        memcpy(data_array + tail_write_cnt, gs_pm_vm_instance->pm_vm_data_buff, PM_VM_DATA_SIZE * head_write_cnt);
        memcpy(data_array,
               gs_pm_vm_instance->pm_vm_data_buff + (data_info->data_cnt - tail_write_cnt),
               PM_VM_DATA_SIZE * tail_write_cnt);
    }

    pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);
    return total_write_cnt;
}

os_err_t pm_vm_device_open()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_adc_dev);

    os_err_t result = OS_SUCCESS;

    /* NOTE: Close/Open/ctl failed, no need to continue */

    /* Open ADC */
    result = os_device_open(gs_pm_vm_instance->pm_vm_adc_dev);
    if (OS_SUCCESS != result)
    {
        ERROR("Voltage monitor's ADC device open failed!");
        return result;
    }

    if ((OS_NULL == gs_pm_vm_instance->pm_vm_temp_dev) ||
        (OS_NULL == gs_pm_vm_instance->optional_cfg.temp_correction_cb))
    {
        /* No need for temp correction */
        return result;
    }

    /* Open temp sensor */
    result = os_device_open(gs_pm_vm_instance->pm_vm_temp_dev);
    if (OS_SUCCESS != result)
    {
        ERROR("Voltage monitor's temp sensor open failed!");
        os_device_close(gs_pm_vm_instance->pm_vm_adc_dev);
    }

    return result;
}

os_err_t pm_vm_device_close()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    os_err_t result = OS_SUCCESS;

    /* Close/Open failed, no need to continue */

    if (OS_NULL != gs_pm_vm_instance->pm_vm_adc_dev)
    {
        result = os_device_close(gs_pm_vm_instance->pm_vm_adc_dev);
        OS_ASSERT_EX(OS_SUCCESS == result, "PM VM ADC device close failed");
    }

    if (OS_NULL != gs_pm_vm_instance->pm_vm_temp_dev)
    {
        result = os_device_close(gs_pm_vm_instance->pm_vm_temp_dev);
        OS_ASSERT_EX(OS_SUCCESS == result, "PM VM temp sensor device close failed");
    }

    return result;
}

os_err_t pm_vm_data_save(int32_t data)
{
    OS_ASSERT(gs_pm_vm_instance->pm_vm_data_info);

#ifdef PM_VM_USING_FAL
    os_err_t result = OS_SUCCESS;
#endif /* PM_VM_USING_FAL */
    uint32_t data_index   = 0;
    uint32_t new_head_pos = gs_pm_vm_instance->pm_vm_data_info->head_pos;
    uint32_t new_data_cnt = gs_pm_vm_instance->pm_vm_data_info->data_cnt;

    if (PM_VM_DATA_MAX > gs_pm_vm_instance->pm_vm_data_info->data_cnt)
    {
        data_index   = gs_pm_vm_instance->pm_vm_data_info->data_cnt;
        new_head_pos = 1;
        new_data_cnt++;
    }
    else
    {
        data_index   = gs_pm_vm_instance->pm_vm_data_info->head_pos - 1;
        new_head_pos = PM_VM_DATA_MAX <= new_head_pos ? 1 : new_head_pos + 1;
    }

    DEBUG("data index:%#X head_pos:%d", data_index, new_head_pos);

    gs_pm_vm_instance->pm_vm_data_buff[data_index] = data;

    /* when success, update fal info part. */
    gs_pm_vm_instance->pm_vm_data_info->head_pos  = new_head_pos;
    gs_pm_vm_instance->pm_vm_data_info->data_cnt  = new_data_cnt;
    gs_pm_vm_instance->pm_vm_data_info->check_num = new_data_cnt ^ new_head_pos;

#ifdef PM_VM_USING_FAL
    result = pm_vm_fal_data_save();
    PM_IS_ERRNO_RETURN(result, "FAL write failed.");
#endif /* PM_VM_USING_FAL */

    return OS_SUCCESS;
}

/* clang-format off */
static os_err_t pm_vm_adc_read(int32_t *buf)
{
    OS_ASSERT(OS_NULL != buf);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_adc_dev);

    os_size_t read_cnt = 0;
    os_err_t  result   = OS_SUCCESS;

    result = os_device_control(gs_pm_vm_instance->pm_vm_adc_dev, OS_ADC_CMD_ENABLE, OS_NULL);
    OS_ASSERT_EX(OS_SUCCESS == result, "PM VM ADC device ctl failed.");

    read_cnt = os_device_read_nonblock(gs_pm_vm_instance->pm_vm_adc_dev, 
                                       PM_VM_BATTERY_ADC_CHANNEL, 
                                       buf, 
                                       PM_VM_DATA_SIZE);

    result = os_device_control(gs_pm_vm_instance->pm_vm_adc_dev, OS_ADC_CMD_DISABLE, OS_NULL);
    OS_ASSERT_EX(OS_SUCCESS == result, "PM VM ADC device ctl failed.");

    if (PM_VM_DATA_SIZE != read_cnt)
    {
        ERROR("PM VM get ADC power voltage value: read failed.ret");
        return OS_FAILURE;
    }

    DEBUG("PM VM get ADC power voltage value[%d].", *buf);
    return OS_SUCCESS;
}
/* clang-format on */

static os_err_t pm_vm_temp_read(float *buf)
{
    OS_ASSERT(OS_NULL != buf);

    os_size_t             read_cnt    = 0;
    struct os_sensor_info sensor_info = {0};
    struct os_sensor_data sensor_data = {0};

    os_device_control(gs_pm_vm_instance->pm_vm_temp_dev, OS_SENSOR_CTRL_GET_INFO, &sensor_info);

    read_cnt =
        os_device_read_nonblock(gs_pm_vm_instance->pm_vm_temp_dev, 0, &sensor_data, sizeof(struct os_sensor_data));
    if (sizeof(struct os_sensor_data) != read_cnt)
    {
        ERROR("PM VM get sensor temp: read failed.");
        return OS_EIO;
    }

    switch (sensor_info.unit)
    {
    case OS_SENSOR_UNIT_MDCELSIUS:
        *buf = sensor_data.data.temp / 1000.0;
        break;
    case OS_SENSOR_UNIT_DCELSIUS:
        *buf = sensor_data.data.temp;
        break;
    default:
        ERROR("PM VM get sensor temp: invalid unit.");
        return OS_FAILURE;
    }

    DEBUG("PM VM get sensor temp value[%f].", *buf);
    return OS_SUCCESS;
}

os_err_t pm_vm_adc_sampling(int32_t *volt_buff)
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_adc_dev);

    float    volt_cal = 0; /* calibrated voltage result with float type */
    os_err_t result   = OS_SUCCESS;
    // os_size_t  read_cnt = 0;
    int32_t volt_databuf[PM_VM_SAMPLING_TIMES_PER_DATA] = {0};
    float   temp_databuf[PM_VM_SAMPLING_TIMES_PER_DATA] = {0};
    float   temp_coefficient                            = 0;
    float   temp_sort_buff                              = 0;
    int32_t volt_sort_buff                              = 0;

    pm_vm_cfg_t *cfg = &gs_pm_vm_instance->optional_cfg;

    /* temperature */
    if (OS_NULL != cfg->temp_correction_cb)
    {
        for (int cnt = 0; cnt < PM_VM_SAMPLING_TIMES_PER_DATA; cnt++)
        {
            result = pm_vm_temp_read(&temp_databuf[cnt]);

            if (OS_SUCCESS != result)
                return OS_FAILURE;
        }

        /* Bubble sort */
        for (int i = 0; i < PM_VM_SAMPLING_TIMES_PER_DATA - 1; i++)
        {
            for (int j = 0; j < PM_VM_SAMPLING_TIMES_PER_DATA - 1 - i; j++)
            {
                if (temp_databuf[j] > temp_databuf[j + 1])
                {
                    temp_sort_buff      = temp_databuf[j];
                    temp_databuf[j]     = temp_databuf[j + 1];
                    temp_databuf[j + 1] = temp_sort_buff;
                }
            }
        }

        /* Use as avg data buff */
        temp_databuf[0] = 0;

        /* Remove maximum & minimal data */
        for (int cnt = 1; cnt < (PM_VM_SAMPLING_TIMES_PER_DATA - 1); cnt++)
        {
            temp_databuf[0] += temp_databuf[cnt];
        }
        temp_databuf[0] /= (PM_VM_SAMPLING_TIMES_PER_DATA - 2);
        DEBUG("%s-%d: Temperature avg [%f]", __func__, __LINE__, temp_databuf[0]);

        temp_coefficient = cfg->temp_correction_cb(temp_databuf[0]);
        if (PM_VM_EPSILON >= fabs(temp_coefficient))
        {
            ERROR("Invalid temperature correction callback retval.");
            return OS_INVAL;
        }
        DEBUG("%s-%d: Temperature coefficient[%f]", __func__, __LINE__, temp_coefficient);
    }

    /* voltage */
    for (int cnt = 0; cnt < PM_VM_SAMPLING_TIMES_PER_DATA; cnt++)
    {
        result = pm_vm_adc_read(&volt_databuf[cnt]);

        if (OS_SUCCESS != result)
            return OS_FAILURE;
    }

    for (int i = 0; i < PM_VM_SAMPLING_TIMES_PER_DATA - 1; i++)
    {
        for (int j = 0; j < PM_VM_SAMPLING_TIMES_PER_DATA - 1 - i; j++)
        {
            if (volt_databuf[j] > volt_databuf[j + 1])
            {
                volt_sort_buff      = volt_databuf[j];
                volt_databuf[j]     = volt_databuf[j + 1];
                volt_databuf[j + 1] = volt_sort_buff;
            }
        }
    }

    /* Use as avg data buff */
    volt_databuf[0] = 0;

    /* Remove maximum & minimal data */
    for (int cnt = 1; cnt < (PM_VM_SAMPLING_TIMES_PER_DATA - 1); cnt++)
    {
        volt_databuf[0] += volt_databuf[cnt];
    }
    volt_databuf[0] /= (PM_VM_SAMPLING_TIMES_PER_DATA - 2);
    DEBUG("%s-%d: Voltage avg [%d]", __func__, __LINE__, volt_databuf[0]);

    /* calibrate */
    DEBUG("%s-%d: voltage before calibrated[%d]", __func__, __LINE__, volt_databuf[0]);
    DEBUG("%s-%d: voltage with calibration_coefficient[%f]",
          __func__,
          __LINE__,
          volt_databuf[0] * cfg->calibration_coefficient);

    volt_cal = volt_databuf[0] * (PM_VM_EPSILON >= fabs(temp_coefficient) ? 1 : temp_coefficient) *
                   (PM_VM_EPSILON <= fabs(cfg->calibration_coefficient) ? cfg->calibration_coefficient : 1) +
               cfg->calibration_base;

    DEBUG("%s-%d: voltage after calibrated[%f]", __func__, __LINE__, volt_cal);

    *volt_buff = volt_cal;
    return OS_SUCCESS;
}

void pm_vm_sampling_workflow()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);

    os_err_t  result    = OS_SUCCESS;
    os_bool_t warn_flag = OS_TRUE; /* if voltage not below the threshold, will set it OS_FALSE */

    int32_t volt_buff     = 0;
    int32_t volt_total    = 0;
    int32_t volt_avg      = 0;
    int8_t  reckeck_count = 0;

#ifdef PM_VM_USING_PINCTL
    pm_vm_pinctl_adc_enable();
#endif /* PM_VM_USING_PINCTL */

    result = pm_vm_device_open();
    if (OS_SUCCESS != result)
    {
        ERROR("PM VM device open failed, do nothing.");
#ifdef PM_VM_USING_PINCTL
        pm_vm_pinctl_adc_disable();
#endif /* PM_VM_USING_PINCTL */
        return;
    }

    do
    {
        result = pm_vm_adc_sampling(&volt_buff);
        if (OS_SUCCESS != result)
        {
            pm_vm_device_close();
#ifdef PM_VM_USING_PINCTL
            pm_vm_pinctl_adc_disable();
#endif /* PM_VM_USING_PINCTL */
            return;
        }

        volt_total += volt_buff;
        ++reckeck_count;

        if (gs_pm_vm_instance->voltage_threshold <= volt_buff)
        {
            warn_flag = OS_FALSE;
            break;
        }

    } while (reckeck_count <= PM_VM_SAMPLING_RECHECK_COUNT);

    pm_vm_device_close();
#ifdef PM_VM_USING_PINCTL
    pm_vm_pinctl_adc_disable();
#endif /* PM_VM_USING_PINCTL */

    volt_avg = volt_total / reckeck_count;

    DEBUG("%s-%d: Average voltage [%d]", __func__, __LINE__, volt_avg);

    if (warn_flag && OS_NULL != gs_pm_vm_instance->optional_cfg.alert_notify_cb)
    {
        gs_pm_vm_instance->optional_cfg.alert_notify_cb(volt_avg);
    }

    /* Ignore FAL write result. */
    pm_vm_data_save(volt_avg);
}

void pm_vm_task_main(void *arg)
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_sem);

    os_err_t result = OS_SUCCESS;

    while (1)
    {
        result = os_semaphore_wait(gs_pm_vm_instance->pm_vm_sem, OS_WAIT_FOREVER);
        if (OS_SUCCESS != result)
        {
            INFO("sem_wait result:%d, exit loop.", result);
            break;
        }

        if (OS_NULL == gs_pm_vm_instance->pm_vm_timer)
        {
            INFO("Into exit session, exit loop.");
            break;
        }

        pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);
        pm_vm_sampling_workflow();
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);
    }

    gs_pm_vm_instance->pm_vm_task = OS_NULL;

    INFO("%s-task exit.", __func__);
}

/* clang-format off */
os_err_t pm_vm_task_create()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL == gs_pm_vm_instance->pm_vm_task);

    gs_pm_vm_instance->pm_vm_task = os_task_create(NULL,
                                                   NULL,
                                                   PM_VM_TASK_STACK_SIZE, 
                                                   PM_VM_TAG, 
                                                   pm_vm_task_main, 
                                                   OS_NULL, 
                                                   PM_VM_TASK_PRIORITY);

    PM_IS_NULL_RETURN(gs_pm_vm_instance->pm_vm_task, "PM VM task create failed, no enough memory.", OS_NOMEM);

    gs_pm_vm_instance->pm_vm_sem = os_semaphore_create(NULL, PM_VM_TAG, 0, OS_SEM_MAX_VALUE);
    if (OS_NULL == gs_pm_vm_instance->pm_vm_sem)
    {
        os_task_destroy(gs_pm_vm_instance->pm_vm_task);
        ERROR("PM VM sen create failed, no enough memory.");
        return OS_NOMEM;
    }

    /* os_task_startup always return OS_SUCCESS */
    if (OS_SUCCESS != os_task_startup(gs_pm_vm_instance->pm_vm_task))
    {
        os_semaphore_destroy(gs_pm_vm_instance->pm_vm_sem);
        os_task_destroy(gs_pm_vm_instance->pm_vm_task);
        ERROR("PM VM task start up failed.");
        return OS_FAILURE;
    }
    return OS_SUCCESS;
}
/* clang-format on */

os_err_t pm_vm_task_destroy()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_sem);

    os_err_t result = OS_SUCCESS;

    result = os_semaphore_destroy(gs_pm_vm_instance->pm_vm_sem);
    OS_ASSERT(OS_SUCCESS == result);

    result = os_task_destroy(gs_pm_vm_instance->pm_vm_task);
    OS_ASSERT(OS_SUCCESS == result);

    return result;
}

os_err_t pm_vm_sem_destroy()
{
    OS_ASSERT(OS_NULL != gs_pm_vm_instance);
    OS_ASSERT(OS_NULL != gs_pm_vm_instance->pm_vm_sem);

    os_err_t result = OS_SUCCESS;

    while (OS_NULL != gs_pm_vm_instance->pm_vm_task)
    {
        os_semaphore_post(gs_pm_vm_instance->pm_vm_sem);
        os_task_yield();
    }

    result = os_semaphore_destroy(gs_pm_vm_instance->pm_vm_sem);
    OS_ASSERT(OS_SUCCESS == result);

    return result;
}

static const pm_vm_act_map_t act_map[] = {
    {PM_VM_INSTANCE_CREATE, (pm_vm_func_t)pm_vm_instance_create, OS_NULL},
    {PM_VM_MUTEX_CREATE, (pm_vm_func_t)pm_vm_mutex_create, pm_vm_instance_destroy},
    {PM_VM_CFG_INIT, (pm_vm_func_t)pm_vm_cfg_init, pm_vm_mutex_destroy},
    {PM_VM_TIMER_CREATE, (pm_vm_func_t)pm_vm_timer_create, OS_NULL},
#ifdef PM_VM_USING_FAL
    {PM_VM_FAL_INIT, (pm_vm_func_t)pm_vm_fal_init, pm_vm_timer_destroy},
    {PM_VM_TASK_CREATE, (pm_vm_func_t)pm_vm_task_create, OS_NULL},
#else
    {PM_VM_TASK_CREATE, (pm_vm_func_t)pm_vm_task_create, pm_vm_timer_destroy},
#endif /* PM_VM_USING_FAL */
    {PM_VM_TIMER_START, (pm_vm_func_t)pm_vm_timer_start, pm_vm_task_destroy},
    {PM_VM_INIT_FINISHED, OS_NULL, OS_NULL},
};

int32_t pm_vm_init_do_act(int32_t threshold_mv, pm_vm_cfg_t *optional_cfg)
{
    os_err_t result     = OS_SUCCESS;
    int32_t  init_index = 0;

    do
    {
        OS_ASSERT(OS_NULL != act_map[init_index].do_func);

        if (init_index != PM_VM_CFG_INIT)
        {
            result = act_map[init_index].do_func();
        }
        else
        {
            result = ((os_err_t(*)(int32_t, pm_vm_cfg_t *))act_map[init_index].do_func)(threshold_mv, optional_cfg);
        }

        if (OS_SUCCESS != result)
        {
            break;
        }
    } while (++init_index < PM_VM_INIT_FINISHED);

    return init_index;
}

void pm_vm_init_err_handler(int32_t state)
{
    int32_t init_index = state;

    /* notice: state == 0 */
    while (0 != init_index)
    {
        if (OS_NULL != act_map[init_index].err_func)
        {
            act_map[init_index].err_func();
        }
        --init_index;
    };
}

/* Just temporary for testing, will change start session sooner. */
os_err_t pm_voltage_monitor_on(int32_t threshold_mv, pm_vm_cfg_t *optional_cfg)
{
    os_err_t result       = OS_SUCCESS;
    int32_t  finish_state = PM_VM_INIT_FINISHED;

    finish_state = pm_vm_init_do_act(threshold_mv, optional_cfg);
    if (PM_VM_INIT_FINISHED != finish_state)
    {
        ERROR("Power monitor failed to start.");
        result = OS_FAILURE;
        pm_vm_init_err_handler(finish_state);
    }

    return result;
}

os_err_t pm_voltage_monitor_off()
{
    if (OS_NULL == gs_pm_vm_instance)
    {
        ERROR("Power monitor has already been stopped.");
        return OS_FAILURE;
    }

    pm_vm_timer_destroy();
    pm_vm_sem_destroy();
    pm_vm_mutex_destroy();
    pm_vm_instance_destroy();

    return OS_SUCCESS;
}

os_err_t pm_vm_clear_history()
{
    if (OS_NULL != gs_pm_vm_instance)
    {
        pm_vm_lock(gs_pm_vm_instance->pm_vm_lock);
        memset(gs_pm_vm_instance->pm_vm_data_info, 0, PM_VM_DATA_INFO_SIZE);
#ifdef PM_VM_USING_FAL
        pm_vm_fal_clear_history();
#endif /* PM_VM_USING_FAL */
        pm_vm_unlock(gs_pm_vm_instance->pm_vm_lock);
    }
    else
    {
#ifdef PM_VM_USING_FAL
        fal_part_t *pm_vm_fal_part = fal_part_find(PM_VM_FAL_NAME);
        PM_IS_NULL_RETURN(pm_vm_fal_part, "FAL part name not found.", OS_INVAL);
        if (0 > fal_part_erase_all(pm_vm_fal_part))
        {
            ERROR("FAL clear history failed, fal part erase all failed.");
            return OS_FAILURE;
        }
#else
        ERROR("FAL clear history failed, VM fal function was disabled.");
#endif /* PM_VM_USING_FAL */
    }
    return OS_SUCCESS;
}
