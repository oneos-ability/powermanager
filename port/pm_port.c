/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_port.c
 *
 * @brief       powermanager porting file
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-27   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>

/******************************************************************************
 *                             VoltageMonitor                                 *
 ******************************************************************************/
#ifdef PM_USING_VOLTAGE_MONITOR

#ifdef PM_VM_USING_PINCTL
#include <drv_gpio.h>
#include <os_stddef.h>
#include "pin/pin.h"

/**
 * @note: If need ADC pin control, enable it to getting battery voltage value.
 * @param[in]   void    void
 * @return              void
 * */
void pm_vm_pinctl_adc_enable(void)
{
    /* add your code here */

    /**
     * eg.
     * os_pin_mode(AD_CRT_PIN, PIN_MODE_OUTPUT);
     * os_pin_write(AD_CRT_PIN, PIN_HIGH);
     *
     */
}

/**
 * @note: If need ADC pin control, disable it to saving energy.
 * @param[in]   void    void
 * @return              void
 * */
void pm_vm_pinctl_adc_disable(void)
{
    /* add your code here */

    /**
     * eg.
     * os_pin_mode(AD_CRT_PIN, PIN_MODE_OUTPUT);
     * os_pin_write(AD_CRT_PIN, PIN_LOW);
     *
     */
}
#endif /* PM_VM_USING_PINCTL */

#endif /* PM_USING_VOLTAGE_MONITOR */

/******************************************************************************
 *                           QuantityEstimator                                *
 ******************************************************************************/
#ifdef PM_USING_QUANTITY_ESTIMATOR

/* reserved */

#endif /* PM_USING_QUANTITY_ESTIMATOR */
