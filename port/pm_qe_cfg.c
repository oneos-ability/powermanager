/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        pm_qe_cfg.c
 *
 * @brief       powermanager QuantityEstimator working device list
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-08   OneOS Team      First Version
 ***********************************************************************************************************************
 */

/******************************************************************************
 *                           QuantityEstimator                                *
 ******************************************************************************/

/**
 *  @note: [QuantityEstimator dev list]
 *  @note: Do not remove 'lpm' & 'normal' device!
 *
 *  Fixed:        user can only change working current
 *  Configurable: user can add/del device and change working current */
static const pm_qe_dev_list_t pm_qe_dev_list[] = {
    /* device name,      working current[uA] */

    /* Fixed */
    {"normal", 21000},
    {"lpm", 12},

    /* Configurable */
    {"uart1", 24},
    {"lcd", 12800},
    {"adc1", 3500},

};
